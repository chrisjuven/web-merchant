import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _userInfo: any;

  constructor(private http: HttpClient) {
  }

  /**
   * 是否登录
   * @returns {boolean}
   */
  isLogin(): boolean {
    return (this._userInfo) ? true : false;
  }

  /**
   * 用户登录
   * @param {string} username
   * @param {string} password
   * @returns {any}
   */
  login(username: string, password: string): Observable<any> {
    const param = new HttpParams().set("username", username).set("password", password);
    return this.http.post("/login", param);
  }


  /**
   * 获取用户信息
   * @returns {any}
   */
  get userInfo(): any {
    return this._userInfo;
  }

  /**
   * 设置用户登录信息
   * @param info
   */
  set userInfo(info: any) {
    this._userInfo = info;
  }
}

/**
 * 用户信息
 */
export interface UserInfo {
  id: string;
  token: string;
  name: string;
  email: string;
  avatar?: string;

  [key: string]: any;
}
