import {SettingsService} from '@delon/theme';
import {Component, OnDestroy, Inject, Optional} from '@angular/core';
import {Router} from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {NzMessageService, NzModalService} from 'ng-zorro-antd';
import {
  TokenService,
  DA_SERVICE_TOKEN,
} from '@delon/auth';
import {StartupService} from '@core/startup/startup.service';
import {AuthService} from "@core/services/auth.service";

@Component({
  selector: 'passport-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
  providers: [],
})
export class UserLoginComponent implements OnDestroy {
  form: FormGroup;
  error = '';
  type = 0;
  loading = false;

  constructor(fb: FormBuilder,
              private router: Router,
              public msg: NzMessageService,
              private modalSrv: NzModalService,
              private settingsService: SettingsService,
              @Inject(DA_SERVICE_TOKEN) private tokenService: TokenService,
              private startupSrv: StartupService,
              private authService: AuthService) {
    this.form = fb.group({
      userName: [null, [Validators.required, Validators.minLength(5)]],
      password: [null, Validators.required],
      mobile: [null, [Validators.required, Validators.pattern(/^1\d{10}$/)]],
      captcha: [null, [Validators.required]],
      remember: [true],
    });
    modalSrv.closeAll();
  }

  // region: fields

  get userName() {
    return this.form.controls.userName;
  }

  get password() {
    return this.form.controls.password;
  }

  get mobile() {
    return this.form.controls.mobile;
  }

  get captcha() {
    return this.form.controls.captcha;
  }

  // endregion

  switch(ret: any) {
    this.type = ret.index;
  }

  // region: get captcha

  count = 0;
  interval$: any;

  getCaptcha() {
    this.count = 59;
    this.interval$ = setInterval(() => {
      this.count -= 1;
      if (this.count <= 0) clearInterval(this.interval$);
    }, 1000);
  }

  // endregion

  submit() {
    this.error = '';
    if (this.type === 0) {
      this.userName.markAsDirty();
      this.userName.updateValueAndValidity();
      this.password.markAsDirty();
      this.password.updateValueAndValidity();
      if (this.userName.invalid || this.password.invalid) return;
    } else {
      this.mobile.markAsDirty();
      this.mobile.updateValueAndValidity();
      this.captcha.markAsDirty();
      this.captcha.updateValueAndValidity();
      if (this.mobile.invalid || this.captcha.invalid) return;
    }
    // mock http
    this.loading = true;
    this.authService.login(this.userName.value, this.password.value).subscribe(info => this.loginSucess(info.data), (error) => console.log(error), () => this.loading = false);

  }

  /**
   * 登录成功
   * @param userInfo
   */
  loginSucess(userInfo: any) {
    this.tokenService.set(userInfo);
    this.settingsService.setUser(userInfo);
    this.authService.userInfo = userInfo;

    // 重新获取 StartupService 内容
    this.startupSrv.load().then(() => this.router.navigate(['/']));
  }

  ngOnDestroy(): void {
    if (this.interval$) clearInterval(this.interval$);
  }
}
