import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TradeOrdersComponent} from "./trade-orders/trade-orders.component";
import {RemitOrdersComponent} from "./remit-orders/remit-orders.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {path: '', redirectTo: 'tradeOrders', pathMatch: 'full'},
      {path: 'tradeOrders', component: TradeOrdersComponent},
      {path: 'remitOrders', component: RemitOrdersComponent}
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule {
}
