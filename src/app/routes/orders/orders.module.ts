import {NgModule} from '@angular/core';
import {SharedModule} from '@shared/shared.module';
import {OrdersRoutingModule} from './orders-routing.module';
import {TradeOrdersComponent} from './trade-orders/trade-orders.component';
import {RemitOrdersComponent} from './remit-orders/remit-orders.component';

const COMPONENTS = [TradeOrdersComponent, RemitOrdersComponent];
const COMPONENTS_NOROUNT = [];

@NgModule({
  imports: [
    SharedModule,
    OrdersRoutingModule
  ],
  declarations: [
    ...COMPONENTS,
    ...COMPONENTS_NOROUNT
  ],
  entryComponents: COMPONENTS_NOROUNT
})
export class OrdersModule {
}
