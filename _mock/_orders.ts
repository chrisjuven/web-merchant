
const tradeOrders = {
  pagesize:10,
  pageindex:1,
  total:100,
  data:[
    {id:20180001,amount:100,merchantOrderId:"M1000001",createTime:new Date(),status:"SUCCESS"},
    {id:20180002,amount:200,merchantOrderId:"M1000002",createTime:new Date(),status:"FAILED"}
  ]
}

export const ORDERS = {
  'GET /orders/tradeOrders': {status: 1, msg: "sucess", data: tradeOrders}
}
