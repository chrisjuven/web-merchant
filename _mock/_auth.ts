import {MockRequest, MockStatusError} from "@delon/mock";
import {HttpParams} from "@angular/common/http";

const token = {token: 82468246, name: "chris", id: "100001"};

function login(params: HttpParams) {
  const user = {
    id: 10001,
    token: "123465798",
    name: "chris",
    email: "280563131@qq.com",
    avatar: "./assets/tmp/img/avatar.jpg"
  };
  const userInfo = {status: 1, msg: "sucess", data: user};


  // if (params.get("username") === "admin" && params.get("password") === "123456") {
    return userInfo;
  // }
}

export const AUTH = {
  'POST /login1': (req: MockRequest) => login(req.body),
}
